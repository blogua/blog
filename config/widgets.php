<?php
/*
 * Ключ: краткое название виджета для обращения к нему из шаблона
 * Значение: название класса виджета с пространством имен
 */
return [
    'menu'          => 'App\Widgets\MenuWidget',
    'new-posts'     => 'App\Widgets\NewPostsWidget',
    'related-posts' => 'App\Widgets\RelatedPostsWidget',
    'tags'          => 'App\Widgets\TagsWidget',
    'categories'    => 'App\Widgets\CategoriesWidget',
];