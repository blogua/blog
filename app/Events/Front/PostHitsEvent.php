<?php

namespace App\Events\Front;

use App\Model\Post;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class PostHitsEvent
 * @package App\Events\Front
 */
class PostHitsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * PostHitsEvent constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        if($post->exists)
        {
            $post->hits++;
            $post->save();
        }
    }

    /**
     * Получить каналы, на которых должно транслироваться событие.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('front-post-hits-event');
    }
}
