<?php namespace App\Widgets;

use App\Model\Post;
use App\Widgets\Contract\ContractWidget;

/**
 * Новые посты
 * Class NewPostsWidget
 * @package App\Widgets
 */
class NewPostsWidget implements ContractWidget
{
    private $conf = [
        'category_id'   => 0,
        'limit'         => 5
    ];

    /**
     * NewPostsWidget constructor.
     * @param array $conf
     */
    function __construct($conf = [])
    {
        $this->conf = array_merge($this->conf, $conf);
    }

    public function execute()
    {
        $items = Post::where('status', 1)->orderBy('created_at', 'DESC')->limit($this->conf['limit']);

        if($this->conf['category_id'])
        {
            $items->inCategory($this->conf['category_id']);
        }

        $items = $items->get();

        return view('Widgets::new-posts', [
            'conf'  => (object)$this->conf,
            'items' => $items
        ]);
    }
}