<?php namespace App\Widgets;

use App\Widgets\Contract\ContractWidget;

class MenuWidget implements ContractWidget
{
    private $conf = [];

    function __construct($conf = [])
    {
        $this->conf = array_merge($this->conf, $conf);
    }

    public function execute()
    {
        return view('Widgets::menu', [
            'conf'  => (object)$this->conf,
            'items' => []
        ]);
    }
}