<?php namespace App\Widgets;

use App\Model\Tag;
use App\Widgets\Contract\ContractWidget;

class TagsWidget implements ContractWidget
{
    private $conf = [
        'limit'         => 5
    ];

    function __construct($conf = [])
    {
        $this->conf = array_merge($this->conf, $conf);
    }

    public function execute()
    {
        $items = Tag::limit($this->conf['limit']);


        $items = $items->get();

        return view('Widgets::tags', [
            'conf'  => (object)$this->conf,
            'items' => $items
        ]);
    }
}