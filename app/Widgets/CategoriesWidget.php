<?php namespace App\Widgets;

use App\Model\Category;
use App\Widgets\Contract\ContractWidget;

class CategoriesWidget implements ContractWidget
{
    private $conf = [
        'limit'         => 5
    ];

    function __construct($conf = [])
    {
        $this->conf = array_merge($this->conf, $conf);
    }

    public function execute()
    {
        $items = Category::limit($this->conf['limit']);


        $items = $items->get();

        return view('Widgets::categories', [
            'conf'  => (object)$this->conf,
            'items' => $items
        ]);
    }
}