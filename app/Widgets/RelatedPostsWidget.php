<?php namespace App\Widgets;

use App\Model\Post;
use App\Widgets\Contract\ContractWidget;

/**
 * Похожие статьи
 * Class RelatedPostsWidget
 * @package App\Widgets
 */
class RelatedPostsWidget implements ContractWidget
{
    private $conf = [
        'category_id'   => 0,
        'limit'         => 5
    ];

    /**
     * RelatedPostsWidget constructor.
     * @param array $conf
     */
    function __construct($conf = [])
    {
        $this->conf = array_merge($this->conf, $conf);
    }

    public function execute()
    {

        return view('Widgets::related-posts', [
            'conf'  => (object)$this->conf,
            'items' => []
        ]);
    }
}