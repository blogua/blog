<?php

namespace App\Http\Requests\Back\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'              => 'Название',
            'slug'              => 'Алиас',
            'short_desc'        => 'Краткое описания',
            'content'           => 'Описание',
            'meta_title'        => 'Мета-тег Title',
            'meta_keyword'      => 'Мета-тег Keywords',
            'meta_description'  => 'Мета-тег Description',
            'category_id'       => 'Категория',
        ];
    }

    public function rules()
    {
        return [
            'category_id'       => 'integer|between:0,99999999',
            'author_id'         => 'integer',
            'meta_robots'       => 'integer',
            'image'             => 'string|max:255|nullable',
            'slug'              => 'string|max:255',
            'name'              => 'required|string|max:255',
            'short_desc'        => 'string|max:255|nullable',
            'content'           => 'required|string',
            'meta_title'        => 'string|max:255|nullable',
            'meta_keyword'      => 'string|max:255|nullable',
            'meta_description'  => 'string|max:255|nullable',
            'status'            => 'required|boolean',
        ];
    }

    public function getItem()
    {
        return array_intersect_key ($this->toArray(), $this->rules());
    }
}
