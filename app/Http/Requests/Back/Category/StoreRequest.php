<?php

namespace App\Http\Requests\Back\Category;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Back\Category
 */
class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'              => 'Название',
            'slug'              => 'Алиас',
            'short_desc'        => 'Краткое описания',
            'content'           => 'Описание',
            'meta_title'        => 'Мета-тег Title',
            'meta_keyword'      => 'Мета-тег Keywords',
            'meta_description'  => 'Мета-тег Description',
            'autoSlug'          => 'autoSlug',
        ];
    }

    public function rules()
    {
        return [
            'image'             => 'string|max:255|nullable',
            'slug'              => [
                'required_if:autoSlug,0',
                'max:255',
                'unique:categories,slug',
            ],
            'name'              => 'required|string|max:255',
            'short_desc'        => 'string|max:255|nullable',
            'content'           => 'string|nullable',
            'meta_title'        => 'string|max:255|nullable',
            'meta_keyword'      => 'string|max:255|nullable',
            'meta_description'  => 'string|max:255|nullable',
            'status'            => 'required|boolean',
        ];
    }

    public function getItem()
    {
        return array_intersect_key ($this->toArray(), $this->rules());
    }
}
