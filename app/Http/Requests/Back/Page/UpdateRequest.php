<?php

namespace App\Http\Requests\Back\Page;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name'              => 'Название',
            'slug'              => 'Алиас',
            'content'           => 'Описание',
            'meta_title'        => 'Мета-тег Title',
            'meta_keyword'      => 'Мета-тег Keywords',
            'meta_description'  => 'Мета-тег Description',
        ];
    }

    public function rules()
    {
        return [
            'slug'              => 'string|max:255',
            'name'              => 'required|string|max:255',
            'content'           => 'required|string',
            'meta_title'        => 'string|max:255|nullable',
            'meta_keyword'      => 'string|max:255|nullable',
            'meta_description'  => 'string|max:255|nullable',
            'status'            => 'required|boolean',
        ];
    }

    public function getItem()
    {
        return array_intersect_key ($this->toArray(), $this->rules());
    }
}
