<?php

namespace App\Http\Controllers\Front;

use App\Model\Post;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Категория
 * Class CategoryController
 * @package App\Http\Controllers\Front
 */
class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @param Category $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, Category $post)
    {
        return view('front.categories', [
            'categories' => Category::paginate(15)
        ]);
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, Category $category)
    {
        return view('front.category', [
            'category'  => $category,
            'posts'     => Post::where('status', 1)->orderBy('created_at', 'DESC')->where('category_id', $category->id)->paginate(15)
        ]);
    }
}
