<?php

namespace App\Http\Controllers\Front;

use App\Model\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Тег
 * Class TagController
 * @package App\Http\Controllers\Front
 */
class TagController extends Controller
{
    public function show(Request $request, Tag $tag)
    {
        if(!$tag->exists)
        {
            abort(404, '404 Page Not Found');
        }

        $posts = $tag->posts()->orderBy('created_at', 'DESC')->paginate($request->get('perPage', 15));

        return view('front.tag', [
            'tag'   => $tag,
            'posts' => $posts
        ]);
    }
}
