<?php

namespace App\Http\Controllers\Front;

use App\Model\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Страница
 * Class PageController
 * @package App\Http\Controllers\Front
 */
class PageController extends Controller
{
    public function index(Request $request, Page $page)
    {
        if(!$page->exists || !$page->status)
        {
            abort(404, '404 Page Not Found');
        }

        return view('front.page', [
            'page' => $page
        ]);
    }
}
