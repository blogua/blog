<?php

namespace App\Http\Controllers\Front;

use App\Model\Post;
use Illuminate\Http\Request;
use App\Events\Front\PostHitsEvent;
use App\Http\Controllers\Controller;

/**
 * Пост
 * Class PostController
 * @package App\Http\Controllers\Front
 */
class PostController extends Controller
{
    public function show(Request $request, Post $post)
    {
        if(!$post->exists || !$post->status)
        {
            abort(404, '404 Page Not Found');
        }

        event(new PostHitsEvent($post));

        return view('front.post', [
            'post' => $post
        ]);
    }
}
