<?php

namespace App\Http\Controllers\Back;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Профиль пользователя
 * Class UserProfileController
 * @package App\Http\Controllers\Back
 */
class UserProfileController extends Controller
{
    /**
     * Получаем профиль
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = null;

        if(auth()->id() == $id)
        {
            $item = User::find($id);
        }

        return response()->json([
            'result'        => $item
        ]);
    }

    /**
     * Обновляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $item = $this->validate($request, [
            'first_name'=> 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email'     => 'required|unique:users,email,'.$id
        ], [], [
            'first_name'    => 'Имя',
            'last_name'     => 'Фамилия',
            'email'         => 'Email',
        ]);

        $item = array_intersect_key ($request->toArray(), [
            'first_name'    => 1,
            'last_name'     => 1,
            'email'         => 1
        ]);

        $password = $this->validate($request, [
            'password'=> 'string|nullable|min:8|confirmed',
        ], [], [
            'password'              => 'Пароль',
            'password_confirmation' => 'Повтор пароля',
        ]);

        if($password['password']){
            $item['password'] = bcrypt($password['password']);
        }
        if(auth()->id() == $id)
        {
            User::where('id', $id)->update($item);
        }

        return response()->json([
            'result' => [
                'id' => $id
            ]
        ]);
    }
}
