<?php

namespace App\Http\Controllers\Back;

use App\User;
use App\Model\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Пользователи
 * Class UserController
 * @package App\Http\Controllers\Back
 */
class UserController extends Controller
{
    /**
     * Список
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = User::orderBy('created_at', 'DESC')->with(['role'])->paginate($request->get('perPage', 15));

        return response()->json($items);
    }

    /**
     * Пользователь
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = User::find($id);

        return response()->json([
            'result'        => $item,
            'roles'         => Role::all()
        ]);
    }

    /**
     * Оюновляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $item = $this->validate($request, [
            'first_name'=> 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'role_id'   => 'required|digits_between:1,3',
            'email'     => 'required|unique:users,email,'.$id
        ], [], [
            'first_name'    => 'Имя',
            'last_name'     => 'Фамилия',
            'email'         => 'Email',
            'role_id'       => 'Роль',
        ]);

        $item = array_intersect_key ($request->toArray(), [
            'first_name'    => 1,
            'last_name'     => 1,
            'email'         => 1,
            'role_id'       => 1
        ]);

        $password = $this->validate($request, [
            'password'=> 'string|nullable|min:8|confirmed',
        ], [], [
            'password'              => 'Пароль',
            'password_confirmation' => 'Повтор пароля',
        ]);

        if($password['password']){
            $item['password'] = bcrypt($password['password']);
        }

        User::where('id', $id)->update($item);

        return response()->json([
            'result' => [
                'id' => $id
            ]
        ]);
    }

    /**
     * Создаем
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $item = $this->validate($request, [
            'first_name'=> 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'role_id'   => 'required|digits_between:1,3',
            'email'     => 'required|unique:users,email',
            'password'  => 'required|string|min:8|confirmed'
        ], [], [
            'first_name'            => 'Имя',
            'last_name'             => 'Фамилия',
            'email'                 => 'Email',
            'role_id'               => 'Роль',
            'password'              => 'Пароль',
            'password_confirmation' => 'Повтор пароля',
        ]);

        $item = array_intersect_key ($request->toArray(), [
            'first_name'    => 1,
            'last_name'     => 1,
            'email'         => 1,
            'role_id'       => 1,
            'password'      => 1
        ]);

        $item = User::create($item);

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * Удаляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $item = User::find($id);
        if($item)
        {
            $item->delete();
        }
        return response()->json([
            'result' => $id
        ]);
    }
}
