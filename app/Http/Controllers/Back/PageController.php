<?php

namespace App\Http\Controllers\Back;

use App\Model\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Traits\SluggableTrait;
use App\Http\Requests\Back\Page\StoreRequest;
use App\Http\Requests\Back\Page\UpdateRequest;

/**
 * Страницы
 * Class PageController
 * @package App\Http\Controllers\Back
 */
class PageController extends Controller
{
    use SluggableTrait;

    /**
     * Список страниц
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = Page::orderBy('name', 'ASC')->with('author')->paginate($request->get('perPage', 15));

        return response()->json($items);
    }

    /**
     * Достаем страницу
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = Page::find($id);

        return response()->json([
            'result'        => $item
        ]);
    }

    /**
     * Создаем
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if($autoSlug = $request->get('autoSlug', 0))
        {
            $slug = $this->getUniqueSlug(new Page, $request->get('name', ''));
            $request->merge(['slug' => $slug]);
        }

        $item = $request->getItem();

        $item['author_id'] = auth()->id();

        $item = Page::create($item);

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * Обновляем
     * @param UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        Page::where('id', $id)->update($request->getItem());

        return response()->json([
            'result' => [
                'id'    => $id
            ]
        ]);
    }

    /**
     * Удаляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $item = Page::find($id);
        if($item)
        {
            $item->delete();
        }

        return response()->json([
            'result' => $id
        ]);
    }
}
