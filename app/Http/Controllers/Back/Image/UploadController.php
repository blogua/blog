<?php

namespace App\Http\Controllers\Back\Image;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class UploadController
 * @package App\Http\Controllers\Back\Image
 */
class UploadController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image'
        ]);

        $file = $request->file('file');

        $fileName = sprintf('%s_%s.%s', crc32($file->getFilename()), time(), $file->extension());
        $pach = sprintf('/upload/%s', $request->get('type', 'type'));

        $file->move(public_path($pach), $fileName);

        return response()->json([
            'location' => $pach.'/'.$fileName
        ]);
    }
}
