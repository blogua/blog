<?php

namespace App\Http\Controllers\Back;

use App\Helper\ImageUploadHelper;
use App\Http\Requests\Back\Category\StoreRequest;
use App\Http\Requests\Back\Category\UpdateRequest;
use App\Model\Category;
use App\Model\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Traits\SluggableTrait;

/**
 * Категории
 * Class CategoryController
 * @package App\Http\Controllers\Back
 */
class CategoryController extends Controller
{
    use SluggableTrait;

    /**
     * Список категорий
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = Category::orderBy('created_at', 'DESC')->paginate($request->get('perPage', 15));

        return response()->json($items);
    }

    /**
     * Получить категорию
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = Category::find($id);

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * Создаем категорию
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if($autoSlug = $request->get('autoSlug', 0))
        {
            $slug = $this->getUniqueSlug(new Category, $request->get('name', ''));
            $request->merge(['slug' => $slug]);
        }

        $imageFile = $this->validate($request, [
            'imageFile' => [
                'mimes:jpeg,jpg,jpe,png,gif,bmp',
                'max:500'
            ],
        ], [], [
            'imageFile' => 'Изображения'
        ]);

        if(!empty($imageFile))
        {
            $image = new ImageUploadHelper();
            $src = $image->save($request->file('imageFile'), 'category');

            $request->merge(['image' => $src]);
        }

        $item = $request->getItem();

        $item = Category::create($item);

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * Обновляем категорию
     * @param UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $imageFile = $this->validate($request, [
            'imageFile' => [
                'mimes:jpeg,jpg,jpe,png,gif,bmp',
                'max:500'
            ],
        ], [], [
            'imageFile' => 'Изображения'
        ]);

        if(!empty($imageFile))
        {
            $image = new ImageUploadHelper();
            $src = $image->save($request->file('imageFile'), 'category');

            $request->merge(['image' => $src]);
        }

        Category::where('id', $id)->update($request->getItem());

        return response()->json([
            'result' => [
                'id' => $id
            ]
        ]);
    }

    /**
     * Удаляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $item = Category::find($id);

        if($item)
        {
            if($item->image)
            {
                /* Удаляем изображения */
                ImageUploadHelper::destroy($item->image);
            }

            Post::where('category_id', $item->id)->update(['category_id' => 0]);

            $item->delete();
        }
        return response()->json([
            'result' => $id
        ]);
    }
}
