<?php

namespace App\Http\Controllers\Back\Appearance;

use App\Model\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Пункты меню
 * Class MenuItemController
 * @package App\Http\Controllers\Back\Appearance
 */
class MenuItemController extends Controller
{
    /**
     * @param Request $request
     * @param int $parent_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $parent_id = 0)
    {
        $items = Menu::where('parent_id', $parent_id)->orderBy('sort', 'ASC')->get();

        return response()->json($items);
    }

    /**
     * @param Request $request
     * @param int $parent_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $parent_id, $id)
    {
        $menu = Menu::find($parent_id);
        $item = Menu::find($id);

        return response()->json([
            'item'        => $item,
            'menu'        => $menu
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $item = $this->validate($request, [
            'name'      => 'required|string|max:255',
            'url'       => 'required|string|max:255',
            'parent_id' => 'required|digits_between:1,99999',
            'status'    => 'required|digits_between:0,1'
        ], [], [
            'name'      => 'Название',
            'url'       => 'URL',
            'status'    => 'Статус',
            'parent_id' => 'parentId',
        ]);

        $item = Menu::create($item);

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * @param Request $request
     * @param int $parent_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $parent_id, $id)
    {
        $item = $this->validate($request, [
            'name'      => 'required|string|max:255',
            'url'       => 'required|string|max:255',
            'status'    => 'required|digits_between:0,1'
        ], [], [
            'name'      => 'Название',
            'url'       => 'URL',
            'status'    => 'Статус',
        ]);
        $item = array_intersect_key ($request->toArray(), [
            'name'      => 1,
            'url'       => 1,
            'status'    => 1
        ]);

        Menu::where('id', $id)->update($item);

        return response()->json([
            'result' => [
                'id'    => $id
            ]
        ]);
    }

    /**
     * @param Request $request
     * @param int $parent_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $parent_id, $id)
    {
        $item = Menu::find($id);
        if($item)
        {
            $item->delete();
        }

        return response()->json([
            'result' => $id
        ]);
    }
}
