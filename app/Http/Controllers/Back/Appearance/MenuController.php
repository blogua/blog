<?php

namespace App\Http\Controllers\Back\Appearance;

use App\Model\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Управления меню
 * Class MenuController
 * @package App\Http\Controllers\Back\Appearance
 */
class MenuController extends Controller
{
    /**
     * Список меню
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = Menu::where('parent_id', 0)->sort($request->get('order'), $request->get('by'))
            ->paginate($request->get('perPage', 15));

        return response()->json($items);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = Menu::find($id);

        return response()->json([
            'result'        => $item
        ]);
    }

    /**
     * Создать елемент
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $item = $this->validate($request, [
            'name'      => 'required|string|max:255',
            'url'       => 'min:0|max:255|string|nullable',
            'parent_id' => 'required|digits_between:0,99999',
            'status'    => 'required|digits_between:0,1'
        ], [], [
            'name'      => 'Название',
            'url'       => 'URL',
            'status'    => 'Статус',
            'parent_id' => 'parentId',
        ]);

        $item = Menu::create($item);

        return response()->json([
            'result' => $item
        ]);
    }

    public function update(Request $request, $id)
    {
        $item = $this->validate($request, [
            'name'      => 'required|string|max:255',
            'url'       => 'min:0|max:255|string|nullable',
            'parent_id' => 'required|digits_between:0,99999',
            'status'    => 'required|digits_between:0,1'
        ], [], [
            'name'      => 'Название',
            'url'       => 'URL',
            'status'    => 'Статус',
            'parent_id' => 'parentId',
        ]);
        $item = array_intersect_key ($request->toArray(), [
            'name'      => 1,
            'url'       => 1,
            'parent_id' => 1,
            'status'    => 1
        ]);

        Menu::where('id', $id)->update($item);

        return response()->json([
            'result' => [
                'id'    => $id
            ]
        ]);
    }

    /**
     * Удаляем меню
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $item = Menu::find($id);
        if($item)
        {
            Menu::where('parent_id', $item->id)->delete();
            $item->delete();
        }

        return response()->json([
            'result' => $id
        ]);
    }
}
