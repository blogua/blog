<?php

namespace App\Http\Controllers\Back;

use App\Model\Tag;
use App\Model\Post;
use App\Model\MapTag;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Helper\ImageUploadHelper;
use App\Http\Controllers\Controller;
use App\Helper\Traits\SluggableTrait;
use Illuminate\Database\QueryException;
use App\Http\Requests\Back\Post\StoreRequest;
use App\Http\Requests\Back\Post\UpdateRequest;

/**
 * Посты
 * Class PostController
 * @package App\Http\Controllers\Back
 */
class PostController extends Controller
{
    use SluggableTrait;

    /**
     * Список
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = Post::orderBy('created_at', 'DESC')->with('author')->paginate($request->get('perPage', 15));

        return response()->json($items);
    }

    /**
     * Пост
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = Post::with(['tags','author'])->find($id);
        $categories = Category::orderBy('name', 'ASC')->get(['id', 'name']);
        $tags = Tag::orderBy('name', 'ASC')->get();

        return response()->json([
            'result'        => $item,
            'tags'          => $tags,
            'categories'    => $categories,
        ]);
    }

    /**
     * Обновляем
     * @param UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $imageFile = $this->validate($request, [
            'imageFile' => [
                'mimes:jpeg,jpg,jpe,png,gif,bmp',
                'max:500'
            ],
        ], [], [
            'imageFile' => 'Изображения'
        ]);

        if(!empty($imageFile))
        {
            $image = new ImageUploadHelper();
            $src = $image->save($request->file('imageFile'), 'post');

            $request->merge(['image' => $src]);
        }

        Post::where('id', $id)->update($request->getItem());

        MapTag::where('post_id', $id)->delete();

        foreach ($request->get('tags', []) as $tag){
            try {
                MapTag::create([
                    'tag_id'    => $tag['id'],
                    'post_id'   => $id
                ]);
            } catch (QueryException $e) { }
        }

        return response()->json([
            'result' => [
                'id'    => $id,
                'tags' => $request->get('tags', [])
            ]
        ]);
    }

    /**
     * Создаем
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        if($autoSlug = $request->get('autoSlug', 0))
        {
            $slug = $this->getUniqueSlug(new Post, $request->get('name', ''));
            $request->merge(['slug' => $slug]);
        }

        $imageFile = $this->validate($request, [
            'imageFile' => [
                'mimes:jpeg,jpg,jpe,png,gif,bmp',
                'max:500'
            ],
        ], [], [
            'imageFile' => 'Изображения'
        ]);

        if(!empty($imageFile))
        {
            $image = new ImageUploadHelper();
            $src = $image->save($request->file('imageFile'), 'post');

            $request->merge(['image' => $src]);
        }

        $item = $request->getItem();

        $item['author_id'] = auth()->id();

        $item = Post::create($item);

        MapTag::where('post_id', $item->id)->delete();

        foreach ($request->get('tags', []) as $tag){
            try {
                MapTag::create([
                    'tag_id'    => $tag['id'],
                    'post_id'   => $item->id
                ]);
            } catch (QueryException $e) { }
        }

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * Удаляем пост
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $item = Post::find($id);
        if($item)
        {
            if($item->image)
            {
                /* Удаляем изображения */
                ImageUploadHelper::destroy($item->image);
            }
            MapTag::where('post_id', $item->id)->delete();
            $item->delete();
        }

        return response()->json([
            'result' => $id
        ]);
    }
}
