<?php

namespace App\Http\Controllers\Back;

use App\Helper\Traits\SluggableTrait;
use App\Model\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Теги
 * Class TagController
 * @package App\Http\Controllers\Back
 */
class TagController extends Controller
{
    use SluggableTrait;

    /**
     * Список
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $items = Tag::orderBy('name', 'ASC')->paginate($request->get('perPage', 15));

        return response()->json($items);
    }

    /**
     * Тег
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $item = Tag::find($id);

        return response()->json([
            'result'        => $item
        ]);
    }

    /**
     * Создаем
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if($autoSlug = $request->get('autoSlug', 0))
        {
            $slug = $this->getUniqueSlug(new Tag, $request->get('name', ''));
            $request->merge(['slug' => $slug]);
        }

        $item = $this->validate($request, [
            'name'  => 'required|string|max:255',
            'slug'  => [
                'required_if:autoSlug,0',
                'max:255',
                'unique:tags,slug',
            ],
        ], [], [
            'name'  => 'Название',
            'slug'  => 'Алиас',
        ]);

        $item = array_intersect_key ($request->toArray(), [
            'name'  => 1,
            'slug'  => 1
        ]);

        $item = Tag::create($item);

        return response()->json([
            'result' => $item
        ]);
    }

    /**
     * Обновляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $item = $this->validate($request, [
            'name'  => 'required|string|max:255',
            'slug'  => [
                'required_if:autoSlug,0',
                'max:255',
                'unique:tags,slug,'.$id,
            ],
        ], [], [
            'name'  => 'Название',
            'slug'  => 'Алиас',
        ]);

        $item = array_intersect_key ($request->toArray(), [
            'name'  => 1,
            'slug'  => 1
        ]);

        Tag::where('id', $id)->update($item);

        return response()->json([
            'result' => [
                'id' => $id
            ]
        ]);
    }

    /**
     * Удаляем
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $item = Tag::find($id);
        if($item)
        {
            $item->delete();
        }
        return response()->json([
            'result' => $id
        ]);
    }
}
