<?php

namespace App\Http\Controllers\Back;

use App\Model\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Общие настройки
 * Class SettingController
 * @package App\Http\Controllers\Back
 */
class SettingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $keys = $request->get('key', '');
        $keys = explode(',', $keys);
        $items = Setting::whereIn('name', $keys)->pluck('value', 'name');;

        return response()->json([
            'result' => $items
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $item = $this->validate($request, [
            'name'  => 'required|string|max:255',
            'value'  => 'string|max:255',
        ]);

        Setting::where('name', $request->get('name'))->update(['value' => $request->get('value')]);

        return response()->json([
            'result' => $item
        ]);
    }
}
