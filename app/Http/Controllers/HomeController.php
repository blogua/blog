<?php

namespace App\Http\Controllers;

use App\Model\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(){ }

    public function index()
    {
        $posts = Post::where('status', 1)->orderBy('created_at', 'DESC')->with('tags')->take(20)->get();
        //dd($posts);
        return view('home', [
            'posts' => $posts
        ]);
    }
}
