<?php

namespace App\Helper;

use Illuminate\Support\Facades\Storage;

/**
 * Управления катринками
 * Class ImageUploadHelper
 * @package App\Helper
 */
class ImageUploadHelper
{
    /**
     * Созраняем IMG
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $dir
     */
    public function save($file, $dir)
    {

        $fileName = sprintf('%s_%s.%s', crc32($file->getFilename()), time(), $file->extension());

        $pach = sprintf('/upload/%s', $dir);

        $file->move(public_path($pach), $fileName);

        return $pach . '/' . $fileName;
    }

    /**
     * Удаляем IMG
     * @param static $src
     * @return bool
     */
    public static function destroy($src)
    {
        if(Storage::disk('etna')->exists($src))
        {
            return Storage::disk('etna')->delete($src);
        }

        return true;
    }
}
