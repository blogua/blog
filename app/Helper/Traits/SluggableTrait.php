<?php

namespace App\Helper\Traits;

/**
 * Уникализация slug
 * Trait SluggableTrait
 * @package App\Helper\Traits
 */
trait SluggableTrait
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $value
     * @param int $ignoreId
     * @return string
     */
    public function getUniqueSlug(\Illuminate\Database\Eloquent\Model $model, $value, $ignoreId = 0)
    {
        $slug = str_slug($value, '-');

        $slugs = $model->where('slug', $slug)->where('id', '!=', $ignoreId)->pluck('slug');

        if(!$slugs->count()) {
            return $slug;
        }

        $slugs = $model->whereRaw("slug REGEXP '^{$slug}(-[0-9]+)?$' AND id != '{$ignoreId}'")->get();

        if ($slugs->count()) {
            return "{$slug}-{$slugs->count()}";
        }

        return "{$slug}-1";
    }
}
