<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'author_id',
        'image',
        'slug',
        'name',
        'short_desc',
        'content',
        'status',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'status',
    ];

    protected $hidden = [];

    /**
     * Получить ключ маршрута для модели.
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = !empty($value) ? $value : null;
    }

    public function setShortDescAttribute($value)
    {
        $this->attributes['short_desc'] = !empty($value) ? $value : null;
    }

    public function setContentAttribute($value)
    {
        $this->attributes['content'] = !empty($value) ? $value : null;
    }

    public function setMetaTitleAttribute($value)
    {
        $this->attributes['meta_title'] = !empty($value) ? $value : null;
    }

    public function setMetaKeywordAttribute($value)
    {
        $this->attributes['meta_keyword'] = !empty($value) ? $value : null;
    }

    public function setMetaDescriptionAttribute($value)
    {
        $this->attributes['meta_description'] = !empty($value) ? $value : null;
    }
}
