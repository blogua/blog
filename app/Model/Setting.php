<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 * @package App\Model
 */
class Setting extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'value',
    ];

    protected $hidden = [];
}
