<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App\Model
 */
class Tag extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'slug',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function posts()
    {
        return $this->belongsToMany(
            'App\Model\Post',
            'map_tags',
            'tag_id',
            'post_id',
            'id'
        );
    }
}
