<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App\Model
 */
class Post extends Model
{
    protected $fillable = [
        'category_id',
        'author_id',
        'image',
        'slug',
        'name',
        'short_desc',
        'content',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'status',
        'hits',
        'meta_robots',
    ];

    protected $hidden = [];

    /**
     * Получить ключ маршрута для модели.
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeInCategory($query, $category_id)
    {
        return $query->where('category_id', $category_id);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = !empty($value) ? $value : null;
    }

    public function setShortDescAttribute($value)
    {
        $this->attributes['short_desc'] = !empty($value) ? $value : null;
    }

    public function setContentAttribute($value)
    {
        $this->attributes['content'] = !empty($value) ? $value : null;
    }

    public function getMetaTitleAttribute($value)
    {
        return !is_null($value) ? $value : '';
    }

    public function setMetaTitleAttribute($value)
    {
        $this->attributes['meta_title'] = !empty($value) ? $value : null;
    }

    public function getMetaKeywordAttribute($value)
    {
        return $value ? $value : '';
    }

    public function setMetaKeywordAttribute($value)
    {
        $this->attributes['meta_keyword'] = !empty($value) ? $value : null;
    }

    public function getMetaDescriptionAttribute($value)
    {
        return $value ? $value : '';
    }

    public function setMetaDescriptionAttribute($value)
    {
        $this->attributes['meta_description'] = !empty($value) ? $value : null;
    }

    public function setMetaRobotsAttribute($value)
    {
        $this->attributes['meta_robots'] = !empty($value) ? $value : 0;
    }

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id');
    }

    /**
     * Теги поста
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(
            'App\Model\Tag',
            'map_tags',
            'post_id',
            'tag_id',
            'id'
        );
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }
}
