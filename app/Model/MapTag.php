<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MapTag
 * @package App\Model
 */
class MapTag extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'tag_id',
        'post_id',
    ];
}
