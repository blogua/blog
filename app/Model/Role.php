<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App\Model
 */
class Role extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
    ];
}
