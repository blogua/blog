<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 * @package App\Model
 */
class Menu extends Model
{
    protected $fillable = [
        'id',
        'parent_id',
        'name',
        'url',
        'status',
        'sort',
    ];

    public function scopeSort($query, $order = 'id', $by = 'asc')
    {
        if(array_search($order, $this->fillable) === false){
            $order = 'id';
        }
        if(array_search($by, ['asc','desc']) === false){
            $by = 'asc';
        }
        return $query->orderBy($order, $by);
    }
}
