<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/check-session', function (Request $request) {
    return $request->user();
});


Route::middleware('auth')->group(function () {
    Route::apiResource('category', 'Back\CategoryController');
    Route::apiResource('post', 'Back\PostController');
    Route::apiResource('tag', 'Back\TagController');
    Route::apiResource('page', 'Back\PageController');
    Route::apiResource('user', 'Back\UserController');
    Route::apiResource('user-profile', 'Back\UserProfileController');
    Route::apiResource('setting', 'Back\SettingController', [
        'only' => [
            'index',
            'store'
        ],
    ]);
    Route::prefix('appearance')->group(function () {
        Route::apiResource('menu', 'Back\Appearance\MenuController');
        Route::apiResource('{id}/menu-item', 'Back\Appearance\MenuItemController');
    });
    Route::apiResource('image/upload', 'Back\Image\UploadController');
});