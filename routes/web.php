<?php
/**
 * @see \Illuminate\Routing\Router
 */

Auth::routes();

Route::middleware('auth')->prefix('admin')->group(function () {
    Route::get('{slug1?}/{slug2?}/{slug3?}/{slug4?}/{slug5?}/{slug6?}', function (...$arg) {
        return view('layouts.ng-back-end');
    });
});



//Illuminate\Routing\PendingResourceRegistration::name(),
Route::resource('post', 'Front\PostController', [
    'only' => [
        'show'
    ],
    'parameters' => [
        'post' => 'post_slug'
    ]
]);

Route::resource('category', 'Front\CategoryController', [
    'only' => [
        'index', 'show'
    ],
    'parameters' => [
        'category' => 'category_slug'
    ]
]);

Route::resource('tag', 'Front\TagController', [
    'only' => [
        'show'
    ],
    'parameters' => [
        'post' => 'tag_slug'
    ]
]);

Route::resource('{page_slug}', 'Front\PageController', [
    'only' => [
        'index'
    ]
]);

Route::get('/', 'HomeController@index')->name('home');

