
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */
try {
    window.jQuery = window.$ = require('jquery/dist/jquery.slim');
    window.Popper = require('popper.js').default;
} catch (e) {}

require('./bootstrap');

let token = document.head.querySelector('meta[name="csrf-token"]');
