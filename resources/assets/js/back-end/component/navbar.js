'use strict';

angular.module('adminApp')
    .controller('NavBarController', ['$scope', '$rootScope', 'UserSession', ($scope, $rootScope, UserSession) => {
        console.log('NavBarController');

        $scope.user = {};

        $scope.logout = () => {
            console.log('logout')
        };

        $scope.$watch(function(){
            return UserSession.auth;
        }, function(newValue, oldValue){
            $scope.user = newValue;
        });
    }])
    .component('navBar', {
        templateUrl: '/views/component/navbar/index.html',
        controller: 'NavBarController'
    });

