'use strict';

angular.module('adminApp')
    .controller('SidebarMainController', ['$scope', '$rootScope', ($scope, $rootScope) => {
        console.log('SidebarMainController');
    }])
    .component('sidebarMain', {
        templateUrl: '/views/component/sidebar-main/index.html',
        controller: 'SidebarMainController'
    });

