/**
 * @example <input type="file" ng-model="form.file" files-input name="file" required>
 */
'use strict';

angular.module('adminApp')
    .directive("filesInput", () => {
        return {
            require: "ngModel",
            link: (scope, elem, attrs, ngModel) => {
                elem.on("change", function (e) {
                    let files = elem[0].files[0];
                    ngModel.$setViewValue(files);
                })
            }
        }
    });