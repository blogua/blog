'use strict';

angular.module('adminApp')
    .directive('sort', () => {
        return {
            restrict: 'A',
            template: '<a href ng-click="setSort(sort)">' +
            '<i ng-if="sort" class="fa" ng-class="listClass()"></i> ' +
            '<span ng-transclude></span>' +
            '</a>',
            transclude: true,
            scope: {
                sort: '=',
                ngModel: '='
            },
            require: ['?ngModel'],
            link: (scope, element, attrs, ctrls) => {

                let sort = {
                    order: null,
                    by: null
                };

                scope.ngModel = scope.ngModel || {};
                scope.ngModel = angular.merge(sort, scope.ngModel);

                scope.sort = scope.sort || null;

                scope.listClass = () => {
                    return {
                        'fa-sort': scope.ngModel.order !== scope.sort,
                        'fa-sort-asc': scope.ngModel.order === scope.sort && scope.ngModel.by === 'asc',
                        'fa-sort-desc': scope.ngModel.order === scope.sort && scope.ngModel.by === 'desc'
                    };
                };

                scope.setSort = (order) => {

                    if(scope.ngModel.order === order)
                    {
                        switch (scope.ngModel.by) {
                            case 'asc':
                                scope.ngModel.by = 'desc';
                                break;
                            case 'desc':
                                scope.ngModel.by = 'asc';
                                break;
                            default:
                                scope.ngModel.by = 'asc';
                                break;
                        }
                    } else {
                        scope.ngModel.by = 'asc';
                    }

                    scope.ngModel.order = order;

                    scope.$parent.init(1);
                };
            }
        }
    })