'use strict';

angular.module('adminApp')
    .directive('progress', ['$rootScope', ($rootScope) => {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="progress" ng-if="showBlockHttpProgress > 0"><div class="indeterminate"></div> </div>',
            link: (scope, element, attr) => {

                scope.showBlockHttpProgress = $rootScope.showBlockHttpProgress || 0;

                $rootScope.$watch('showBlockHttpProgress', (newVal, oldVal) => {
                    scope.showBlockHttpProgress = newVal;
                });

                let color = attr.color || null;

                if (color) {
                    element.addClass('color-' + color)
                }
            }
        }
    }]);