'use strict';

angular.module('adminApp')
    .directive('convertToNumber', () => {
        return {
            require: 'ngModel',
            link: (scope, element, attrs, ngModel) => {
                ngModel.$parsers.push((val) => {
                    return parseInt(val, 10);
                });
                ngModel.$formatters.push((val) => {
                    return '' + val;
                });
            }
        };
    })