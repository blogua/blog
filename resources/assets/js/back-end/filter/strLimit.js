'use strict';

angular.module('adminApp')
    .filter('strLimit', ['$filter', function ($filter) {
        return function (input, limit, end) {
            if (!input) return;
            if (input.length <= limit) {
                return input;
            }
            end = end || '...';
            return $filter('limitTo')(input, limit) + end;
        };
    }]);