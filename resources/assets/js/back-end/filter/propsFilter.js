'use strict';
/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs an AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform an OR.
 */
angular.module('adminApp')
    .filter('propsFilter', () => {
        return (items, props) => {
            let out = [];

            if (angular.isArray(items)) {
                let keys = Object.keys(props);

                items.forEach((item) => {
                    let itemMatches = false;

                    for (let i = 0; i < keys.length; i++) {
                        let prop = keys[i];
                        let text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });