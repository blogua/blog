'use strict';

angular.module('adminApp')
    .config(["$provide", "$httpProvider", ($provide, $httpProvider) => {
        $httpProvider.interceptors.push('blockHttpInterceptor');
    }])
    .factory('blockHttpInterceptor', ['$q','$rootScope','inform', ($q, $rootScope, inform) => {

        $rootScope.showBlockHttpProgress = 0;

        let block = {};

        function error(rejection) {
            try {
                /* Вывод сообщения об ошибке */
                if(rejection.data && rejection.data.message && angular.isString(rejection.data.message))
                {
                    if(rejection.status === 422 && rejection.data.errors && angular.isObject(rejection.data.errors))
                    {
                        inform.add('Ошибка заполнения формы.', {
                            ttl: 4000,
                            type: 'warning'
                        });
                    } else {
                        inform.add(rejection.data.message, {
                            ttl: 10000,
                            type: 'danger'
                        });
                    }
                }
                if (block[rejection.config.url])
                {
                    delete block[rejection.config.url];
                    $rootScope.showBlockHttpProgress--;
                }
            } catch (ex) {
                console.log('httpRequestError', ex);
            }

            return $q.reject(rejection);
        }

        return {
            request: (config) => {

                if (config.url.search('/api/') === 0 && !block[config.url]) {

                    block[config.url] = true;

                    $rootScope.showBlockHttpProgress++;
                }

                return config;
            },
            requestError: error,
            response: (response) => {
                if (block[response.config.url])
                {
                    delete block[response.config.url];
                    $rootScope.showBlockHttpProgress--;
                }

                return response;
            },
            responseError: error
        };
    }]);