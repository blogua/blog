/**
 * @example
 *  PasswordGenerator()
 */
'use strict';

angular.module('adminApp')
    .factory('PasswordGenerator', [() => {

        let lowerCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
            upperCharacters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
            numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
            specials = ['!', '"', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '}', '~'];

        let option = {
            passwordLength: 10,
            prefix: '',
            uppercase: true,
            numbers: true,
            specials: true,
        };

        return (opt) => {
            opt = opt || {};

            opt = angular.merge(option, opt);

            let finalCharacters = lowerCharacters;

            if (option.uppercase) {
                finalCharacters = finalCharacters.concat(upperCharacters);
            }

            if (option.numbers) {
                finalCharacters = finalCharacters.concat(numbers);
            }

            if (option.specials) {
                finalCharacters = finalCharacters.concat(specials);
            }

            let finalPassword = [];

            for (let i = 0; i < option.passwordLength; i++) {
                finalPassword.push(finalCharacters[Math.floor(Math.random() * finalCharacters.length)]);
            }

            return option.prefix + finalPassword.join('');
        };
    }]);
