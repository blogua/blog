/**
 * @example
 *  UserSession
 */
'use strict';

angular.module('adminApp')
    .factory('UserSession', ['$resource','$urlRouter',($resource, $urlRouter) => {

        let Resource = $resource('/api/check-session');

        let Auth = {
            auth: null,
            checkSession: () => {
                let check = false;
                if (!Auth.auth) {
                    Resource.get({}, (response) => {
                        Auth.auth = response;
                        $urlRouter.sync();
                    });
                } else {
                    check = true;
                }
                return check
            },
            canAdmin: () => {
                return (!_.isNull(Auth.auth) && Auth.auth.role_id === 1);
            },
            canAuthor: () => {
                return (!_.isNull(Auth.auth) && Auth.auth.role_id === 3);
            },
            canEditor: () => {
                return (!_.isNull(Auth.auth) && Auth.auth.role_id === 2);
            }
        };

        return  Auth
    }]);
