/**
 * @example
 *  HttpFormData.put(url, data);
 *  HttpFormData.post(url, data);
 */
'use strict';

angular.module('adminApp')
    .factory('HttpFormData', ['$http', ($http) => {

        let fd = (data, put) => {

            let formData = new FormData();

            let run = {
                obj: (data, previousKey) => {

                    Object.keys(data).forEach(key => {
                        if(!key.startsWith("$", 0))
                        {
                            const value = data[key];
                            if (previousKey) {
                                key = `${previousKey}[${key}]`;
                            }
                            run.is(value, key);
                        }
                    });
                },
                arr: (value, previousKey) => {
                    value.forEach((val, key) => {
                        if (previousKey) {
                            key = `${previousKey}[${key}]`;
                        }
                        run.is(val, key);
                    });
                },
                is: (value, key) => {

                    if(value instanceof Array) {

                        run.arr(value, key);

                    }  else if (key && value instanceof File) {

                        formData.append(key, new Blob([value], {type: undefined}));

                    } else if (value instanceof Object) {

                        run.obj(value, key);

                    } else if (key && value instanceof String) {

                        formData.append(key, value);

                    } else if (key && value instanceof Number) {

                        formData.append(key, value);

                    } else if(key) {

                        formData.append(key, value);

                    }
                }
            };

            run.is(data);

            if(window.csrfHeader) {
                formData.append('_token', window.csrfHeader);
            }
            if(put) {
                formData.append('_method', 'PUT');
            }

            return formData;
        };

        let getHttp = (requestUrl, requestData, put) => {
            put = put || false;

            return $http({
                method: 'POST',
                url: requestUrl,
                data: fd(requestData, put),
                withCredentials: true,
                headers: {
                    'Content-Type': undefined
                }
            })
        };

        return {
            post: (requestUrl, requestData) => {
                return getHttp(requestUrl, requestData);
            },
            put: (requestUrl, requestData) => {
                return getHttp(requestUrl, requestData, true);
            }
        };
    }]);