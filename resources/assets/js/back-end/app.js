/**
 * https://lodash.com/
 * @type {_}
 */
window._ = require('lodash/lang');
/**
 * https://docs.angularjs.org/api
 * @type {angular}
 */
window.angular = require('angular');
/**
 * https://oclazyload.readme.io/docs
 */
require('oclazyload');
/**
 * https://github.com/Narzerus/angular-permission/wiki/Managing-permissions
 */
require('angular-permission');
require('angular-animate');
require('angular-sanitize');
require('angular-resource');

require('angular-ui-router');
require('angular-ui-router/lib/legacy/stateEvents');
/**
 * https://angular-ui.github.io/bootstrap/
 */
require('angular-ui-bootstrap');
/**
 * https://github.com/ncuillery/angular-breadcrumb
 */
require('angular-breadcrumb');
/**
 * https://github.com/McNull/angular-inform
 */
require('angular-inform');

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.csrfHeader = token.content;
} else {
    console.error('CSRF token not found');
}

angular.module('adminApp', [
    'oc.lazyLoad',
    'ngAnimate',
    'ngSanitize',
    'ngResource',
    'ui.router',
    'permission',
    'permission.ui',
    'ui.router.state.events',
    'ui.bootstrap',
    'ncy-angular-breadcrumb',
    'inform'
]).config(['$compileProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider', '$resourceProvider', 'uibDatepickerConfig', '$permissionProvider',
    ($compileProvider, $locationProvider, $stateProvider, $urlRouterProvider, $resourceProvider, uibDatepickerConfig, $permissionProvider) => {

        //$permissionProvider.suppressUndefinedPermissionWarning(true);

        $resourceProvider.defaults.withCredentials = true;
        $resourceProvider.defaults.actions = {
            post: {method: 'POST'},
            get: {method: 'GET'},
            getAll: {method: 'GET', isArray: true},
            //update: {method: 'PUT'},
            delete: {method: 'DELETE'},
            /**
             * CRUD-роуты
             * GET|HEAD  | api/patch      | Controller@index
             * POST      | api/patch      | Controller@store
             * GET|HEAD  | api/patch/{id} | Controller@show
             * PUT|PATCH | api/patch/{id} | Controller@update
             * DELETE    | api/patch/{id} | Controller@destroy
             */
            index: {method: 'GET'},
            store: {method: 'POST'},
            show: {method: 'GET'},
            update: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        };
        $resourceProvider.defaults.actions = {
            post: {method: 'POST'},
            get: {method: 'GET'},
            getAll: {method: 'GET', isArray: true},
            //update: {method: 'PUT'},
            delete: {method: 'DELETE'},
            /**
             * CRUD-роуты
             * GET|HEAD  | api/patch      | Controller@index
             * POST      | api/patch      | Controller@store
             * GET|HEAD  | api/patch/{id} | Controller@show
             * PUT|PATCH | api/patch/{id} | Controller@update
             * DELETE    | api/patch/{id} | Controller@destroy
             */
            index: {method: 'GET'},
            store: {method: 'POST'},
            show: {method: 'GET'},
            update: {method: 'PUT'},
            destroy: {method: 'DELETE'}
        };

        $locationProvider.html5Mode(true);
        $urlRouterProvider.when('', '/admin');
        //$urlRouterProvider.when('', '/');
        $compileProvider.debugInfoEnabled(false);
        $compileProvider.commentDirectivesEnabled(false);
        //$compileProvider.cssClassDirectivesEnabled(false);

        $stateProvider.state('app', {
            abstract: true,
            url: '/admin',
            template: '<ui-view />',
            data: {
                permissions: {
                    only: 'AUTHORIZED'
                }
            }
        }).state('app.dashboard', {
            url: '',
            templateUrl: '/views/dashboard.html',
            ncyBreadcrumb: {
                label: 'Dashboard'
            }
        });

        $stateProvider.state('404', {
            templateUrl: '/views/notfound.html',
            ncyBreadcrumb: {
                label: 'Oops!'
            }
        });

        // показать не найденную ошибку, если маршрут неизвестен, но сохранить исходный url
        $urlRouterProvider.otherwise(function($injector) {
            $injector.invoke(function($state) {
                $state.transitionTo("404", {}, false);
            });
        });

        /**
         * @linck https://angular-ui.github.io/bootstrap/#!#datepicker
         */
        uibDatepickerConfig.startingDay = 1;
        uibDatepickerConfig.showWeeks = false;
        uibDatepickerConfig.datepickerPopup = "dd-MM-yyyy+";
        uibDatepickerConfig.currentText = "Oggi+";
        uibDatepickerConfig.clearText = "Pulisci+";
        uibDatepickerConfig.closeText = "Chiudi+";
    }])
    .run(['$rootScope', '$state', '$stateParams', 'PermRoleStore', 'PermPermissionStore', ($rootScope, $state, $stateParams, PermRoleStore, PermPermissionStore) => {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        PermPermissionStore.definePermission('canEditor', ['UserSession', (Session) => {
            console.log('canEditor')
                return Session.canEditor();
        }]);
        PermPermissionStore.definePermission('canAuthor', ['UserSession', (Session) => {
            console.log('canAuthor')
            return Session.canAuthor();
        }]);
        PermPermissionStore.definePermission('canAdmin', ['UserSession', (Session) => {
            console.log('canAdmin')
            return Session.canAdmin();
        }]);

        PermRoleStore.defineManyRoles({
            'AUTHORIZED': ['UserSession', (UserSession) => {
                return UserSession.checkSession();
            }],
            'EDITOR': ['canEditor'],
            'AUTHOR': ['canAuthor'],
            'ADMIN': ['canAdmin']
        });
    }])
    .constant('Conf', {
        currentPage: 1,
        perPage: 15,
    })
    .constant('uibPaginationConfig', {
        itemsPerPage: 15,
        boundaryLinks: true,
        boundaryLinkNumbers: false,
        directionLinks: false,
        firstText: 'Первая',
        previousText: 'Предыдущая',
        nextText: 'Следующая',
        lastText: 'Последняя',
        rotate: true,
        forceEllipses: true
    })
    .constant('uibDatepickerPopupConfig', {
        altInputFormats: [],
        appendToBody: false,
        clearText: 'Очистить',
        closeOnDateSelection: true,
        closeText: 'Готово',
        currentText: 'Cегодня',
        datepickerPopup: 'yyyy-MM-dd',
        datepickerPopupTemplateUrl: 'uib/template/datepickerPopup/popup.html',
        datepickerTemplateUrl: 'uib/template/datepicker/datepicker.html',
        html5Types: {
            date: 'yyyy-MM-dd',
            'datetime-local': 'yyyy-MM-ddTHH:mm:ss.sss',
            'month': 'yyyy-MM'
        },
        onOpenFocus: true,
        showButtonBar: false,
        placement: 'auto bottom-left'
    })
    .controller('BaseController', ['$rootScope','$scope', ($rootScope, $scope) => {
        console.log('BaseController');

        $scope.copy = new Date();
    }]);

require('./filter');
require('./factory');
require('./provider');
require('./service');
require('./controller');
require('./component');
require('./directive');
