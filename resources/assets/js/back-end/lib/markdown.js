/**
 * @link https://markdown-it.github.io/
 * @link https://github.com/markdown-it/markdown-it
 * @link https://markdown-it.github.io/markdown-it/#MarkdownIt.prototype.renderer
 */
'use strict';

// require('markdown-it');
window.MarkDown = require('markdown-it');