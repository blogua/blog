/**
 * Controller
 */
require('./controller/category/category')
require('./controller/post/post')
require('./controller/tag/tag')
require('./controller/page/page')
require('./controller/user/user')
require('./controller/setting/setting')
require('./controller/appearance/appearance')