/**
 *  Внешний вид - Меню
 */
'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.appearance.menu', {
            url: '/menu',
            views: {
                'panel': {
                    templateUrl: '/views/controller/appearance/menu/index.html',
                    controller: 'AppearanceMenuController',
                }
            },
            ncyBreadcrumb: {
                label: 'Меню',
                parent: 'app.dashboard'
            }
        }).state('app.appearance.menuForm', {
            url: '/menu/form/{id:[0-9]{1,6}}',
            views: {
                'panel': {
                    controller: 'AppearanceMenuFormController',
                    templateUrl: '/views/controller/appearance/menu/form.html',
                }
            },
            ncyBreadcrumb: {
                label: 'Редактировать меню',
                parent: 'app.appearance.menu'
            }
        }).state('app.appearance.menuItem', {
            url: '/menu/{id:[0-9]{1,6}}',
            views: {
                'panel': {
                    controller: 'AppearanceMenuItemsController',
                    templateUrl: '/views/controller/appearance/menu/item.html',
                }
            },
            ncyBreadcrumb: {
                label: 'Пункт меню',
                parent: 'app.appearance.menu'
            }
        }).state('app.appearance.menuItemForm', {
            url: '/menu/{parent_id:[0-9]{1,6}}/form/{id:[0-9]{1,6}}',
            views: {
                'panel': {
                    controller: 'AppearanceMenuItemFormController',
                    templateUrl: '/views/controller/appearance/menu/item-form.html',
                }
            },
            ncyBreadcrumb: {
                label: 'Пункт меню',
                parent: 'app.appearance.menu'
            }
        });
    }])
    .controller('AppearanceMenuController', ['$scope', '$resource', 'Conf', ($scope, $resource, Conf) => {
        console.log('AppearanceMenuController');

        $scope.perPage = Conf.perPage;
        $scope.result = {};

        let Resource = $resource('/api/appearance/menu');

        $scope.sort = {
            order: 'id',
            by: 'asc'
        };

        $scope.init = (page) => {
            page = page || 1;

            Resource.get({
                page: page,
                order: $scope.sort.order,
                by: $scope.sort.by
            }, (response) => {
                $scope.result = response;
            })
        };

        $scope.init(1);
    }])
    .controller('AppearanceMenuFormController', ['$scope', '$resource', '$state', '$stateParams', 'inform',
        ($scope, $resource, $state, $stateParams, inform) => {

            console.log('AppearanceMenuFormController');

            let Resource = $resource('/api/appearance/menu/:id', {
                id: '@id'
            });

            let formId = $stateParams.id || 0;

            $scope.errors = {};

            $scope.form = {
                name: '',
                parent_id: 0,
                url: '',
                status: 1,
                sort: 0,
            };

            $scope.getItem = () => {
                Resource.show({
                    id: formId
                }, (response) => {
                    if (response.result) {
                        $scope.form = angular.merge($scope.form, response.result);
                    } else {
                        $state.go('app.appearance.menu');
                    }
                })
            };

            if (isFinite(formId) && formId !== '0') {
                $scope.getItem();
            }

            $scope.submit = () => {
                console.log('submit')

                $scope.errors = {};

                let form;

                if ($scope.form.id) {
                    form = Resource.update($scope.form);
                } else {
                    form = Resource.store($scope.form);
                }

                form.$promise.then((response) => {
                    action.id = action.id === -1 ? response.result.id : action.id;
                    $state.go(action.go, {
                        id: action.id.toString()
                    }, {reload: true});

                    inform.add('Форма сохранена.', {type: 'success'})
                }, (response) => {
                    console.log(response)
                    $scope.errors = response.data.errors;
                });

            };
            let action = {
                go: 'app.appearance.menu',
                id: 0
            };
            $scope.setActon = (go, id) => {
                action.go = go;
                action.id = id || 0;
            };

            $scope.destroy = () => {
                if ($scope.form.id) {
                    Resource.destroy({
                        id: $scope.form.id
                    }, (response) => {
                        $state.go('app.appearance.menu');
                    })
                }
            }
        }])
    .controller('AppearanceMenuItemsController', ['$scope', '$resource', '$state', '$stateParams', 'inform',
        ($scope, $resource, $state, $stateParams, inform) => {

            console.log('AppearanceMenuItemsController');


            let Resource = $resource('/api/appearance/:id/menu-item/:itemId', {
                id: '@id',
                itemId: '@itemId'
            });

            $scope.menuId = $stateParams.id || 0;

            $scope.init = () => {
                Resource.getAll({
                    id: $scope.menuId
                },(response) => {
                    $scope.result = response;
                })
            };

            $scope.init();
        }])
    .controller('AppearanceMenuItemFormController', ['$scope', '$resource', '$state', 'inform',
        ($scope, $resource, $state, inform) => {
            console.log('AppearanceMenuItemFormController');
            console.log($scope);

            let $stateParams = $scope.$resolve.$stateParams || {};;

            let Resource = $resource('/api/appearance/:parent_id/menu-item/:id', {
                parent_id: '@parent_id',
                id: '@id'
            });

            $scope.errors = {};

            let parentId = $stateParams.parent_id || 0;
            let formId = $stateParams.id || 0;

            $scope.menu = {};

            $scope.form = {
                name: '',
                parent_id: parentId,
                url: '',
                status: 1,
                sort: 0,
            };

            $scope.getItem = () => {
                Resource.show({
                    parent_id: parentId,
                    id: formId
                }, (response) => {
                    if (response.menu) {
                        $scope.menu = response.menu;
                    } else {
                        $state.go('app.appearance.menu');
                    }
                    if (response.item) {
                        $scope.form = angular.merge($scope.form, response.item);
                    }
                })
            };

            if (isFinite(formId) && isFinite(parentId)) {
                $scope.getItem();
            }

            $scope.submit = () => {
                console.log('submit')

                $scope.errors = {};

                let form;

                if ($scope.form.id) {
                    form = Resource.update($scope.form);
                } else {
                    form = Resource.store($scope.form);
                }

                form.$promise.then((response) => {
                    $state.go('app.appearance.menuItem', {id: parentId})
                    inform.add('Форма сохранена.', {type: 'success'})
                }, (response) => {
                    console.log(response)
                    $scope.errors = response.data.errors;
                });

            };

            $scope.destroy = () => {
                if ($scope.form.id) {
                    Resource.destroy({
                        parent_id: parentId,
                        id: $scope.form.id
                    }, (response) => {
                        $state.go('app.appearance.menuItem', {id: parentId})
                    })
                }
            }
        }]);