/**
 *  Внешний вид - Виджеты
 */
'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.appearance.widget', {
            url: '/widget',
            views: {
                'panel': {
                    templateUrl: '/views/controller/appearance/widget/index.html',
                    controller: 'AppearanceWidgetController',
                }
            },
            ncyBreadcrumb: {
                label: 'Виджеты',
                parent: 'app.dashboard'
            }
        });
    }])
    .controller('AppearanceWidgetController', ['$scope', ($scope) => {
        console.log('AppearanceWidgetController');
    }]);