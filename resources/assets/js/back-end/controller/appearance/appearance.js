/**
 *  Внешний вид
 */
'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.appearance', {
            abstract: true,
            url: '/appearance',
            templateUrl: '/views/controller/appearance/index.html',
            controller: 'AppearanceController',
            data: {
                permissions: {
                    only: ['ADMIN']
                }
            }
        });
    }])
    .controller('AppearanceController', ['$scope', ($scope) => {
        console.log('AppearanceController');
    }]);

require('./menu')
require('./widget')