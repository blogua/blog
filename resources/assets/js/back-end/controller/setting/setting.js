'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.setting', {
            abstract: true,
            url: '/setting',
            template: '<div ui-view/>',
            controller: 'SettingController',
            data: {
                permissions: {
                    only: ['ADMIN']
                }
            },
            resolve: {
                loadMyService: ['$ocLazyLoad', ($ocLazyLoad) => {
                    return $ocLazyLoad.load([
                        '/js/lib/xeditable.js',
                        '/css/lib/xeditable.css',
                    ]);
                }]
            },
        }).state('app.setting.general', {
            url: '',
            templateUrl: '/views/controller/setting/general.html',
            controller: 'SettingGeneralController',
            ncyBreadcrumb: {
                label: 'Общие настройки',
                parent: 'app.dashboard'
            }
        });
    }])
    .controller('SettingController', ['$scope','editableOptions', ($scope, editableOptions) => {
        console.log('SettingController');

        editableOptions.theme = 'bs3';
        editableOptions.icon_set = 'font-awesome';
        editableOptions.submitButtonTitle = 'Submit';
        editableOptions.submitButtonAriaLabel = 'Submit';
        editableOptions.cancelButtonTitle = 'Cancel';
        editableOptions.cancelButtonAriaLabel = 'Cancel';
        editableOptions.clearButtonTitle = 'Clear';
        editableOptions.clearButtonAriaLabel = 'Clear';
        editableOptions.displayClearButton = false;

    }])
    .controller('SettingGeneralController', ['$scope','$resource','inform', ($scope, $resource, inform) => {
        console.log('SettingGeneralController');

        let Resource = $resource('/api/setting');

        $scope.form = {
            name: '',
            description: ''
        };

        Resource.index({
            key: 'name,description'
        }, (response) => {
            if(response.result)
            {
                $scope.form = angular.merge($scope.form, response.result);
            }
        });

        $scope.update = (name, value) => {
            console.log('name:', name, '|value:', value)
            //$scope.form[name] = value;
            Resource.store({
                name: name,
                value: value
            }, (response) => {
                $scope.form[name] = value;
                inform.add('Настройки сохранены.', {type: 'success'})
            })
        }
    }]);