'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.user', {
            abstract: true,
            url: '/user',
            template: '<div ui-view/>',
            controller: 'UserController'
        }).state('app.user.list', {
            url: '',
            templateUrl: '/views/controller/user/index.html',
            controller: 'UserListController',
            ncyBreadcrumb: {
                label: 'Пользователи',
                parent: 'app.dashboard'
            },
            data: {
                permissions: {
                    only: 'ADMIN'
                }
            }
        }).state('app.user.form', {
            url: '/form/{id:[0-9]{1,6}}',
            controller: 'UserFormController',
            templateUrl: '/views/controller/user/form.html',
            ncyBreadcrumb: {
                label: 'Редактировать',
                parent: 'app.user.list'
            },
            data: {
                permissions: {
                    only: ['ADMIN']
                }
            }
        }).state('app.user.profile', {
            url: '/profile',
            controller: 'UserProfileController',
            templateUrl: '/views/controller/user/profile.html',
            ncyBreadcrumb: {
                label: 'Профиль',
                parent: 'app.dashboard'
            }
        });
    }])
    .controller('UserController', ['$scope', ($scope) => {
        console.log('TagController');
    }])
    .controller('UserProfileController', ['$scope','$resource','inform','UserSession','PasswordGenerator', ($scope,$resource,inform,UserSession,PasswordGenerator) => {
        console.log('UserProfileController');
        $scope.errors = {};
        $scope.form = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            password_confirmation: ''
        };

        let Resource = $resource('/api/user-profile/:id', {
            id: '@id'
        });

        $scope.getItem = () => {
            Resource.show({
                id: UserSession.auth.id
            }, (response) => {
                if (!response.result)
                {
                    $state.go('app.dashboard');
                } else {
                    $scope.form = angular.merge($scope.form, response.result);
                }
            })
        };
        $scope.getItem();

        $scope.submit = () => {
            console.log('submit')

            $scope.errors = {};

            Resource.update($scope.form).$promise.then((response) => {
                inform.add('Форма сохранена.', {type: 'success'})
            }, (response) => {
                console.log(response)
                $scope.errors = response.data.errors;
            });

        };

        $scope.createPassword = () => {
            $scope.form.password_confirmation = $scope.form.password = PasswordGenerator();
        }
    }])
    .controller('UserFormController', ['$scope','$resource','$stateParams','HttpFormData','$state','inform','PasswordGenerator',
        ($scope,$resource,$stateParams,HttpFormData,$state,inform, PasswordGenerator) => {
            console.log('UserFormController');
            console.log(PasswordGenerator());

            let Resource = $resource('/api/user/:id', {
                id: '@id'
            });

            let formId = $stateParams.id || 0;

            $scope.errors = {};
            $scope.roles = [];

            $scope.form = {
                role_id: 3,
                first_name: '',
                last_name: '',
                email: '',
                password: '',
                password_confirmation: ''
            };

            $scope.getItem = () => {
                Resource.show({
                    id: formId
                }, (response) => {
                    if (formId !== '0' && !response.result)
                    {
                        $state.go('app.user.list');
                    } else {
                        $scope.form = angular.merge($scope.form, response.result);
                        $scope.roles = response.roles;
                    }
                })
            };
            if(isFinite(formId)) {
                $scope.getItem();
            }


            let action = {
                go: 'app.post.list',
                id: 0
            };

            $scope.submit = () => {
                console.log('submit')

                $scope.errors = {};

                let form;

                if($scope.form.id) {
                    form = Resource.update($scope.form);
                } else {
                    form = Resource.store($scope.form);
                }

                form.$promise.then((response) => {
                    action.id = action.id===-1 ? response.result.id : action.id;
                    $state.go(action.go, {
                        id: action.id.toString()
                    }, {reload: true});

                    inform.add('Форма сохранена.', {type: 'success'})
                }, (response) => {
                    console.log(response)
                    $scope.errors = response.data.errors;
                });

            };

            $scope.setActon = (go, id) => {
                action.go = go;
                action.id = id || 0;
            };

            $scope.destroy = () => {
                if($scope.form.id)
                {
                    Resource.destroy({
                        id: $scope.form.id
                    }, (response) => {
                        $state.go('app.user.list');
                    })
                }
            };

            $scope.createPassword = () => {
                $scope.form.password_confirmation = $scope.form.password = PasswordGenerator();
            }
        }])
    .controller('UserListController', ['$scope','$resource','Conf', ($scope, $resource, Conf) => {
        console.log('UserListController');

        $scope.currentPage = Conf.currentPage;
        $scope.perPage = Conf.perPage;
        $scope.result = {};

        let Resource = $resource('/api/user');

        $scope.init = (page, perPage) => {
            $scope.currentPage = page = page || $scope.currentPage;
            $scope.perPage = perPage = perPage || $scope.perPage;

            Resource.get({
                page: page,
                perPage: perPage
            }, (response) => {
                $scope.result = response;
            })
        };

        $scope.init();
    }]);