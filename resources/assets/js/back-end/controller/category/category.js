'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.category', {
            abstract: true,
            url: '/category',
            template: '<div ui-view/>',
            controller: 'CategoryController',
            data: {
                permissions: {
                    only: ['ADMIN','EDITOR']
                }
            }
        }).state('app.category.list', {
            url: '',
            templateUrl: '/views/controller/category/index.html',
            controller: 'CategoryListController',
            ncyBreadcrumb: {
                label: 'Категории',
                parent: 'app.dashboard'
            }
        }).state('app.category.form', {
            url: '/form/{id:[0-9]{1,6}}',
            controller: 'CategoryFormController',
            templateUrl: '/views/controller/category/form.html',
            ncyBreadcrumb: {
                label: 'Редактировать',
                parent: 'app.category.list'
            },
            resolve: {
                loadMyService: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        '/js/lib/tinymce/tinymce.min.js',
                        '/js/lib/tinymce/ui-tinymce.min.js',
                    ]);
                }]
            },
        });
    }])
    .controller('CategoryController', ['$scope','$resource', ($scope, $resource) => {
        console.log('CategoryController');
    }])
    .controller('CategoryFormController', ['$scope','$resource','$state','$stateParams','$http','HttpFormData','inform',
        ($scope, $resource, $state, $stateParams, $http, HttpFormData, inform) => {

        console.log('CategoryFormController');

        let Resource = $resource('/api/category/:id/:action', {
            id: '@id',
            action: '@action'
        });
        let formId = $stateParams.id || 0;

        $scope.errors = {};

        $scope.form = {
            name: '',
            slug: '',
            short_desc: '',
            content: '',
            meta_title: '',
            meta_keyword: '',
            imageFile: {},
            image: '',
            meta_description: '',
            status: 0,
            autoSlug: 1
        };

        $scope.getItem = () => {
            Resource.show({
                id: formId
            }, (response) => {
                if(response.result)
                {
                    $scope.form = angular.merge($scope.form, response.result)
                }else {
                    $state.go('app.category.list');
                }
            })
        };
        if(isFinite(formId) && formId !== '0') {
            $scope.getItem();
        }

        $scope.tinymceOptions = {
            inline: false,
            language: 'ru',
            plugins : 'advlist autolink link table image lists importcss preview fullscreen code',
            menubar: false,
            toolbar: 'undo redo | bold italic | styleselect fontsizeselect | cut copy paste | alignleft aligncenter alignright | numlist bullis outdent indent | table | link unlink image | preview code fullscreen',
            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Responsive', value: 'img-responsive'},
                {title: 'Rounded', value: 'img-rounded'},
                {title: 'Circle', value: 'img-circle'},
                {title: 'Thumbnail', value: 'img-thumbnail'},
            ],
            importcss_append: true,
            content_css: [
                '/css/app.css'
            ],
            table_class_list: [
                {title: 'None', value: ''},
                {title: 'table', value: 'table'},
                {title: 'table-striped', value: 'table table-striped'},
                {title: 'table-bordered', value: 'table table-bordered'},
                {title: 'table-hover', value: 'table table-hover'},
                {title: 'table-condensed', value: 'table table-condensed'},
            ],
            table_row_class_list: [
                {title: 'None', value: ''},
                {title: 'active', value: 'active'},
                {title: 'success', value: 'success'},
                {title: 'warning', value: 'warning'},
                {title: 'danger', value: 'danger'},
                {title: 'info', value: 'info'},
            ],
            image_title: true,
            image_caption: false,
            image_description: false,
            automatic_uploads: true,
            file_picker_types: 'image',
            images_upload_url: '/api/image/upload',
            images_upload_handler: function (blobInfo, success, failure) {
                let xhr, formData;
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '/api/image/upload');
                xhr.onload = () => {
                    let json;
                    if (xhr.status !== 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location !== 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    success(json.location);
                };
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                formData.append('_token', window.csrfHeader);
                formData.append('type', 'category');
                xhr.send(formData);
            },
            relative_urls : false,
            remove_script_host : true,
            convert_urls: false,
            min_height: 400,
            plugin_base_urls: '/js/lib/tinymce/plugins',
            skin_url: '/js/lib/tinymce/skins/lightgray',
        };

        let action = {
            go: 'app.category.list',
            id: 0
        };

        $scope.submit = () => {
            console.log('submit')
            //console.log($scope.form)

            $scope.errors = {};

            let form;
            let method = 'POST';
            let url = '/api/category';

            if($scope.form.id) {
                form = HttpFormData.put('/api/category/'+$scope.form.id, $scope.form);
            } else {
                form = HttpFormData.post('/api/category', $scope.form);
            }

            form.then((response) => {
                action.id = action.id===-1 ? response.data.result.id : action.id;
                $state.go(action.go, {
                    id: action.id.toString()
                }, {reload: true});

                inform.add('Форма сохранена.', {type: 'success'})
            }, (response) => {
                console.log(response)
                $scope.errors = response.data.errors;
            });

        };

        $scope.setActon = (go, id) => {
            action.go = go;
            action.id = id || 0;
        };

        $scope.destroy = () => {
            if($scope.form.id)
            {
                Resource.destroy({
                    id: $scope.form.id
                }, (response) => {
                    $state.go('app.category.list');
                })
            }
        }
    }])
    .controller('CategoryListController', ['$scope','$resource','Conf', ($scope, $resource, Conf) => {
        console.log('CategoryListController');

        let Resource = $resource('/api/category/:id/:action', {
            id: '@id',
            action: '@action'
        });

        $scope.currentPage = Conf.currentPage;
        $scope.perPage = Conf.perPage;
        $scope.result = {};

        $scope.init = (page, perPage) => {
            $scope.currentPage = page = page || $scope.currentPage;
            $scope.perPage = perPage = perPage || $scope.perPage;

            Resource.index({
                page: page,
                perPage: perPage
            }, (response) => {
                $scope.result = response;
            })
        };

        $scope.init();
    }]);