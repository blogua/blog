'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.post', {
            abstract: true,
            url: '/post',
            template: '<div ui-view/>',
            controller: 'PostController',
            data: {
                permissions: {
                    only: ['ADMIN','EDITOR','AUTHOR']
                }
            }
        }).state('app.post.list', {
            url: '',
            templateUrl: '/views/controller/post/index.html',
            controller: 'PostListController',
            ncyBreadcrumb: {
                label: 'Посты', // angular-breadcrumb's configuration
                parent: 'app.dashboard'
            }
        }).state('app.post.form', {
            url: '/form/{id:[0-9]{1,6}}',
            controller: 'PostFormController',
            templateUrl: '/views/controller/post/form.html',
            ncyBreadcrumb: {
                label: 'Редактировать',
                parent: 'app.post.list'
            },
            resolve: {
                loadMyService: ['$ocLazyLoad', ($ocLazyLoad) => {
                    return $ocLazyLoad.load([
                        '/js/lib/ui-select/select.js',
                        '/js/lib/tinymce/tinymce.min.js',
                        '/js/lib/tinymce/ui-tinymce.min.js',
                    ]);
                }]
            },
        });
    }])
    .controller('PostController', ['$scope', ($scope) => {
        console.log('PostController');
    }])
    .controller('PostFormController', ['$scope','$resource','$stateParams','HttpFormData','$state','inform',
        ($scope,$resource,$stateParams,HttpFormData,$state,inform) => {
        console.log('PostFormController');

        $scope.HOST = (location.protocol) == 'https' ? location.origin : location.host;

        let Resource = $resource('/api/post/:id/:action', {
            id: '@id',
            action: '@action'
        });

        let formId = $stateParams.id || 0;

        $scope.errors = {};
        $scope.categories = [];
        $scope.tags = [];

        $scope.form = {
            category_id: 0,
            name: '',
            slug: '',
            short_desc: '',
            content: '',
            meta_title: '',
            meta_keyword: '',
            imageFile: {},
            image: '',
            meta_description: '',
            status: 0,
            meta_robots: 0,
            hits: 0,
            autoSlug: 1,
            tags: []
        };

        $scope.getItem = () => {
            Resource.show({
                id: formId
            }, (response) => {
                if (formId !== '0' && !response.result)
                {
                    $state.go('app.post.list');
                } else {
                    $scope.form = angular.merge($scope.form, response.result);
                    $scope.categories = response.categories;
                    $scope.tags = response.tags;
                }
            })
        };
        if(isFinite(formId)) {
            $scope.getItem();
        }

        $scope.tinymceOptions = {
            inline: false,
            language: 'ru',
            plugins : 'advlist autolink link table image lists importcss preview fullscreen code',
            menubar: false,
            toolbar: 'undo redo | bold italic | styleselect fontsizeselect | cut copy paste | alignleft aligncenter alignright | numlist bullis outdent indent | table | link unlink image | preview code fullscreen',
            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Responsive', value: 'img-responsive'},
                {title: 'Rounded', value: 'img-rounded'},
                {title: 'Circle', value: 'img-circle'},
                {title: 'Thumbnail', value: 'img-thumbnail'},
            ],
            importcss_append: true,
            content_css: [
                '/css/app.css'
            ],
            table_class_list: [
                {title: 'None', value: ''},
                {title: 'table', value: 'table'},
                {title: 'table-striped', value: 'table table-striped'},
                {title: 'table-bordered', value: 'table table-bordered'},
                {title: 'table-hover', value: 'table table-hover'},
                {title: 'table-condensed', value: 'table table-condensed'},
            ],
            table_row_class_list: [
                {title: 'None', value: ''},
                {title: 'active', value: 'active'},
                {title: 'success', value: 'success'},
                {title: 'warning', value: 'warning'},
                {title: 'danger', value: 'danger'},
                {title: 'info', value: 'info'},
            ],
            image_title: true,
            image_caption: false,
            image_description: false,
            automatic_uploads: true,
            file_picker_types: 'image',
            images_upload_url: '/api/image/upload',
            images_upload_handler: function (blobInfo, success, failure) {
                let xhr, formData;
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '/api/image/upload');
                xhr.onload = () => {
                    let json;
                    if (xhr.status !== 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location !== 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    success(json.location);
                };
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                formData.append('_token', window.csrfHeader);
                formData.append('type', 'post');
                xhr.send(formData);
            },
            relative_urls : false,
            remove_script_host : true,
            convert_urls: false,
            min_height: 400,
            plugin_base_urls: '/js/lib/tinymce/plugins',
            skin_url: '/js/lib/tinymce/skins/lightgray',
        };

        let action = {
            go: 'app.post.list',
            id: 0
        };
        /* Отправляем форму */
        $scope.submit = () => {

            $scope.errors = {};

            let form;
            let method = 'POST';
            let url = '/api/category';

            if($scope.form.id) {
                form = HttpFormData.put('/api/post/'+$scope.form.id, $scope.form);
            } else {
                form = HttpFormData.post('/api/post', $scope.form);
            }

            form.then((response) => {
                action.id = action.id===-1 ? response.data.result.id : action.id;
                $state.go(action.go, {
                    id: action.id.toString()
                }, {reload: true});

                inform.add('Форма сохранена.', {type: 'success'})
            }, (response) => {
                console.log(response)
                $scope.errors = response.data.errors;
            });

        };
        $scope.setActon = (go, id) => {
            action.go = go;
            action.id = id || 0;
        };
        /* Удаляем пост */
        $scope.destroy = () => {
            if($scope.form.id)
            {
                Resource.destroy({
                    id: $scope.form.id
                }, (response) => {
                    $state.go('app.post.list');
                })
            }
        }

        $scope.someGroupFn = function (item){

            if (item.name[0] >= 'A' && item.name[0] <= 'M')
                return 'From A - M';

            if (item.name[0] >= 'N' && item.name[0] <= 'Z')
                return 'From N - Z';

        };
    }])
    .controller('PostListController', ['$scope','$resource','Conf', ($scope, $resource, Conf) => {
        console.log('PostListController');

        $scope.currentPage = Conf.currentPage;
        $scope.perPage = Conf.perPage;
        $scope.result = {};

        let Resource = $resource('/api/post/:id/:action', {
            id: '@id',
            action: '@action'
        }, {withCredentials: true});

        $scope.init = (page, perPage) => {
            $scope.currentPage = page = page || $scope.currentPage;
            $scope.perPage = perPage = perPage || $scope.perPage;

            Resource.get({
                page: page,
                perPage: perPage
            }, (response) => {
                $scope.result = response;
            })
        };

        $scope.init();
    }]);