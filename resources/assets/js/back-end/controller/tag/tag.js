'use strict';

angular.module('adminApp')
    .config(['$stateProvider', ($stateProvider) => {

        $stateProvider.state('app.tag', {
            abstract: true,
            url: '/tag',
            template: '<div ui-view/>',
            controller: 'TagController',
            data: {
                permissions: {
                    only: ['ADMIN','EDITOR']
                }
            }
        }).state('app.tag.list', {
            url: '',
            templateUrl: '/views/controller/tag/index.html',
            controller: 'TagListController',
            ncyBreadcrumb: {
                label: 'Теги',
                parent: 'app.dashboard'
            }
        }).state('app.tag.form', {
            url: '/form/{id:[0-9]{1,6}}',
            controller: 'TagFormController',
            templateUrl: '/views/controller/tag/form.html',
            ncyBreadcrumb: {
                label: 'Редактировать',
                parent: 'app.tag.list'
            }
        });
    }])
    .controller('TagController', ['$scope', ($scope) => {
        console.log('TagController');
    }])
    .controller('TagFormController', ['$scope','$resource','$stateParams','HttpFormData','$state','inform',
        ($scope,$resource,$stateParams,HttpFormData,$state,inform) => {
            console.log('TagFormController');

            let Resource = $resource('/api/tag/:id/:action', {
                id: '@id'
            });

            let formId = $stateParams.id || 0;

            $scope.errors = {};

            $scope.form = {
                name: '',
                slug: '',
                autoSlug: 1
            };

            $scope.getItem = () => {
                Resource.show({
                    id: formId
                }, (response) => {
                    if(response.result)
                    {
                        $scope.form = angular.merge($scope.form, response.result);
                    } else{
                        $state.go('app.tag.list');
                    }
                })
            };
            if(isFinite(formId) && formId !== '0') {
                $scope.getItem();
            }


            let action = {
                go: 'app.post.list',
                id: 0
            };

            $scope.submit = () => {
                console.log('submit')

                $scope.errors = {};

                let form;

                if($scope.form.id) {
                    form = Resource.update($scope.form);
                } else {
                    form = Resource.store($scope.form);
                }

                form.$promise.then((response) => {
                    action.id = action.id===-1 ? response.result.id : action.id;
                    $state.go(action.go, {
                        id: action.id.toString()
                    }, {reload: true});

                    inform.add('Форма сохранена.', {type: 'success'})
                }, (response) => {
                    console.log(response)
                    $scope.errors = response.data.errors;
                });

            };

            $scope.setActon = (go, id) => {
                action.go = go;
                action.id = id || 0;
            };

            $scope.destroy = () => {
                if($scope.form.id)
                {
                    Resource.destroy({
                        id: $scope.form.id
                    }, (response) => {
                        $state.go('app.tag.list');
                    })
                }
            }
        }])
    .controller('TagListController', ['$scope','$resource','Conf', ($scope, $resource, Conf) => {
        console.log('PostListController');

        $scope.currentPage = Conf.currentPage;
        $scope.perPage = Conf.perPage;
        $scope.result = {};

        let Resource = $resource('/api/tag');

        $scope.init = (page, perPage) => {
            $scope.currentPage = page = page || $scope.currentPage;
            $scope.perPage = perPage = perPage || $scope.perPage;

            Resource.get({
                page: page,
                perPage: perPage
            }, (response) => {
                $scope.result = response;
            })
        };

        $scope.init();
    }]);