@extends('layouts.app')

@section('content')
    <article class="container">
        <h1 class="display-1">Oops!</h1>
        <h3 class="display-3">{{ $exception->getMessage() }}</h3>
        <div class="row">
            <p class="lead">We can't seem to find the page you're looking for.</p>
        </div>
    </article>
@endsection
