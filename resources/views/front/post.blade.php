@extends('layouts.app')

@include('front.block.meta.title', ['meta_title' => $post->meta_title])
@include('front.block.meta.description', ['meta_description' => $post->meta_description])
@include('front.block.meta.keyword', ['meta_keyword' => $post->meta_keyword])
@include('front.block.meta.robots', ['meta_robots' => $post->meta_robots])

@section('content')
    <article class="container">
        <h1 class="display-4">{{$post->name}}</h1>
        <div class="row">
            <div class="col-9">
                <nav class="breadcrumb">
                    <a class="breadcrumb-item" href="/">Home</a>
                    <a class="breadcrumb-item" href="/category">Categories</a>
                    @if($post->category)
                    <a class="breadcrumb-item" href="{{route('category.show', ['category_slug' => $post->category->slug], false)}}">{{str_limit($post->category->name, 50)}}</a>
                    @endif
                    <span class="breadcrumb-item active">{{str_limit($post->name, 50)}}</span>
                </nav>
                <div class="article">
                    <div class="tags">
                        @if($post->tags)
                            @foreach ($post->tags as $tag)
                                <a href="/tag/{{$tag->slug}}" class="badge badge-primary">{{$tag->name}}</a>
                            @endforeach
                        @endif
                    </div>
                    <div class="tags">
                        <i class="fa fa-eye" aria-hidden="true"></i> {{$post->hits or 0}}
                    </div>
                    {!! $post->content !!}
                </div>
            </div>
            <div class="col-3">
                @widget('new-posts', ['category_id' => $post->category_id])
            </div>
        </div>
    </article>
@endsection
