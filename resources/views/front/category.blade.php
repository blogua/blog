@extends('layouts.app')

@include('front.block.meta.title', ['meta_title' => $category->meta_title])
@include('front.block.meta.description', ['meta_description' => $category->meta_description])
@include('front.block.meta.keyword', ['meta_keyword' => $category->meta_keyword])

@section('content')
    <div class="container">
        <h1 class="display-4">{{$category->name}}</h1>
        <nav class="breadcrumb">
            <a class="breadcrumb-item" href="/">Home</a>
            <a class="breadcrumb-item" href="/category">Categories</a>
            <span class="breadcrumb-item active">{{str_limit($category->name, 50)}}</span>
        </nav>
        <div class="list-group">
            @foreach ($posts as $post)
                <a href="/post/{{ $post->slug }}" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="row">
                        <div class="col-3">
                            <img src="{{$post->image}}" class="rounded float-left" width="100%" alt="...">
                        </div>
                        <div class="col-9">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{$post->name}}</h5>
                                <small>{{date('d.m.Y', strtotime($post->created_at))}}</small>
                            </div>
                            <p class="mb-1">{{str_limit($post->short_desc, random_int(200, 300))}}</p>
                            <small>Donec id elit non mi porta.</small>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
        {{ $posts->links() }}
    </div>
@endsection
