@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <nav class="w-100 ml-3 mr-3 breadcrumb">
                <a class="breadcrumb-item" href="/">Home</a>
                <span class="breadcrumb-item active">Categories</span>
            </nav>

            @foreach ($categories as $category)
                <div class="w-25 mb-2">
                <div class="card ml-1 mr-1 h-100">
                    <a href="/category/{{ $category->slug }}">
                        <img class="card-img-top" src="{{$category->image}}" alt="{{$category->name}}">
                    </a>
                    <div class="card-body text-left">
                        <h4 class="card-title">
                            <a href="/category/{{$category->slug}}" class="text-muted">
                                {{str_limit($category->name, 50)}}
                            </a>
                        </h4>
                        <p class="card-text">{{str_limit($category->short_desc, 255)}}</p>
                    </div>
                </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container">
        {{ $categories->links() }}
    </div>
@endsection
