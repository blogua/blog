@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
    <article class="container">
        <h1 class="display-4">{{$page->name}}</h1>
        <nav class="breadcrumb">
            <a class="breadcrumb-item" href="/">Home</a>
            <span class="breadcrumb-item active">{{str_limit($page->name, 50)}}</span>
        </nav>
        <div class="row">
            <div class="article">
                {!! $page->content !!}
            </div>
        </div>
    </article>
@endsection
