@extends('layouts.app')

@section('title', $tag->name)

@section('content')
    <div class="container">
        <h1 class="display-4">Тег - <small class="text-muted">{{$tag->name}}</small></h1>
        <nav class="breadcrumb">
            <a class="breadcrumb-item" href="/">Home</a>
            <span class="breadcrumb-item active">{{$tag->name}}</span>
        </nav>
        <div class="list-group">
            @foreach ($posts as $post)
                <a href="/post/{{ $post->slug }}" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="row">
                        <div class="col-3">
                            <img src="{{$post->image}}" class="rounded float-left" width="100%" alt="...">
                        </div>
                        <div class="col-9">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{$post->name}}</h5>
                                <small>{{date('d.m.Y', strtotime($post->created_at))}}</small>
                            </div>
                            <p class="mb-1">{{str_limit($post->short_desc, random_int(200, 300))}}</p>
                            <small>Donec id elit non mi porta.</small>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
    <div class="container">
        {{ $posts->links() }}
    </div>
@endsection
