@section('meta_keyword')
    @empty(!$meta_keyword)
        <meta name="keywords" content="{{$meta_keyword}}">
    @endempty
@endsection