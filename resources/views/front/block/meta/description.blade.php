@section('meta_description')
    @empty(!$meta_description)
        <meta name="description" content="{{$meta_description}}">
    @endempty
@endsection