@section('meta_robots')
    @isset($meta_robots)
        @switch($meta_robots)
            @case(1)
        <meta name="robots" content="index, follow" />
                @break
            @case(2)
        <meta name="robots" content="noindex, follow" />
                @break
            @case(3)
        <meta name="robots" content="index, nofollow" />
                @break
            @case(4)
        <meta name="robots" content="noindex, nofollow" />
                @break
            @endswitch
    @endisset
@endsection