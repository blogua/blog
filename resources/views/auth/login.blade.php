@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-sm-12 col-md-6">
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <h1 class="display-3">Login</h1>
                    @if ($errors->has('email'))
                    <div class="alert alert-danger" role="alert">
                        {{ $errors->first('email') }}
                    </div>
                    @endif
                    @if ($errors->has('password'))
                    <div class="alert alert-danger" role="alert">
                        {{ $errors->first('password') }}
                    </div>
                    @endif
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               placeholder="E-Mail Address"
                               name="email"
                               value="{{ old('email') }}"
                               required autofocus autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="Password"
                               name="password"
                               required autofocus autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Remember Me</span>
                    </label>
                </div>
                <button type="submit" class="btn btn-outline-primary btn-block">Login</button>
                <a href="{{ route('password.request') }}" class="btn btn-outline-secondary btn-block">Forgot Your Password?</a>
            </form>
        </div>
    </div>
</div>
@endsection
