@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-sm-12 col-md-6">
                <form method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <h1 class="display-3">Reset Password</h1>
                        @if ($errors->has('email'))
                            <div class="alert alert-danger" role="alert">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   placeholder="E-Mail Address"
                                   name="email"
                                   value="{{ old('email') }}"
                                   required autofocus autocomplete="off">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary btn-block">Send Password Reset Link</button>
                </form>
            </div>
        </div>
    </div>
@endsection
