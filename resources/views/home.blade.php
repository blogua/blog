@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-9">
            <div class="row">
                @foreach($posts as $post)
                    <div class="w-50 mb-3">
                        <div class="card mr-3 h-100">
                            <a href="/post/{{$post->slug}}">
                                <img class="card-img-top" src="{{$post->image}}" alt="{{$post->name}}">
                            </a>
                            <div class="card-body text-left">
                                <h4 class="card-title">
                                    <a href="/post/{{$post->slug}}" class="text-muted">
                                        {{str_limit($post->name, 50)}}
                                    </a>
                                </h4>
                                <p class="card-text font-italic ">{{str_limit($post->short_desc, 100)}}</p>
                                @if($post->tags)
                                    <p class="card-text">
                                        @foreach($post->tags as $tag)
                                            <a href="/tag/{{$tag->slug}}" class="badge badge-info">{{$tag->name}}</a>
                                        @endforeach
                                    </p>
                                @endif
                                <p class="card-text">
                                    <small class="text-muted">{{date('d.m.Y', strtotime($post->created_at))}}</small>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
        <div class="col-3">
            @widget('categories', ['limit' => 10])
            @widget('tags', ['limit' => 20])
        </div>
    </div>
</div>
@endsection
