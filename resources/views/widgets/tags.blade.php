<p class="display-4">Tags</p>
@foreach ($items as $item)
    <a href="/tag/{{ $item->slug }}" class="btn btn-outline-secondary btn-sm">{{$item->name}}</a>
@endforeach