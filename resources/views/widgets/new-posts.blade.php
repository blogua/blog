<div class="list-group">
    <li class="list-group-item d-flex justify-content-between align-items-center">
        НОВОСТИ
    </li>
    @foreach ($items as $item)
        <a href="/post/{{ $item->slug }}" class="list-group-item list-group-item-action flex-column align-items-start">
            <p class="mb-1">{{str_limit($item->name, 50)}}</p>
            <small class="text-muted">{{date('d.m.Y', strtotime($item->created_at))}}</small>
        </a>
    @endforeach
</div>