<p class="display-4">Categories</p>
<div class="list-group">
    @foreach ($items as $item)
        <a href="/category/{{ $item->slug }}" class="list-group-item list-group-item-action">{{$item->name}}</a>
    @endforeach
</div>