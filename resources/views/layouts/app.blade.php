<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta_title')
    @yield('meta_description')
    @yield('meta_keyword')
    @yield('meta_robots')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="/img/favicon.ico">
    <meta name="robots" content="noindex, nofollow" />
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-tops">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Etna') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTop">
                @widget('menu')
                <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
                    @guest
                        <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->first_name }}
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="#"></a>
                                <button class="dropdown-item btn-link" type="button"
                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Выйти
                                </button>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>

        </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>
    <footer class="footer clearfix bg-dark ">
        <div class="container">
            <div class="row">
                <nav class="nav">
                    <a class="nav-link text-light small" href="/">© {{date('Y')}} Etna</a>
                </nav>
            </div>
        </div>
    </footer>
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
