<!doctype html>
<html lang="{{ app()->getLocale() }}" ng-app="adminApp" ng-cloak>
    <head>
        <meta charset="utf-8">
        <base href="/">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title ncy-breadcrumb-text  ncy-breadcrumb-text-separator=" > ">ENTA</title>

        <link rel="stylesheet" href="{{ mix('/css/ng.css') }}">

        <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">

        <meta name="robots" content="noindex, nofollow" />
    </head>
    <body ng-controller="BaseController" class="app-root" ng-cloak>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2">
                    <aside class="row app-sidebar-main">
                        <div class="leads text-center">
                            <img src="/img/android-icon-48x48.png" style="height: 51px;"> Etna
                        </div>
                        <sidebar-main class="sidebar-main"></sidebar-main>
                    </aside>
                </div>
                <div class="col-md-10 main">
                    <div class="row app-main">
                        <nav-bar class="navBarTop"></nav-bar>
                        <div class="page-header">
                            <h1 class="h2 page-title" ncy-breadcrumb-last></h1>
                            <div ncy-breadcrumb></div>
                        </div>
                        <div ui-view ng-animate-children="" class="col-md-12"></div>
                        <footer>© @{{copy|date:'yyyy'}} Etna.</footer>
                    </div>
                </div>
            </div>
        </div>
        <div inform class="inform-fixed inform-animate"></div>
        <script src="{{ mix('/js/ng.js') }}"></script>
    </body>
</html>
