let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.config.publicPath = 'public';

mix.js('resources/assets/js/back-end/app.js', 'public/js/ng.js').version();
mix.js('resources/assets/js/back-end/lib/markdown.js', 'public/js/lib/markdown.js');

mix.copy('node_modules/ui-select/dist/select.min.js', 'public/js/lib/ui-select/select.js');

/* Tinymce */
mix.copy('node_modules/tinymce/tinymce.min.js', 'public/js/lib/tinymce/tinymce.min.js');
mix.copy('node_modules/angular-ui-tinymce/dist/tinymce.min.js', 'public/js/lib/tinymce/ui-tinymce.min.js');
mix.copyDirectory('node_modules/tinymce/skins', 'public/js/lib/tinymce/skins');
mix.copyDirectory('node_modules/tinymce/plugins', 'public/js/lib/tinymce/plugins');
mix.copyDirectory('node_modules/tinymce/themes', 'public/js/lib/tinymce/themes');
mix.copyDirectory('resources/assets/js/back-end/lib/tinymce/langs', 'public/js/lib/tinymce/langs');

/* xeditable */
mix.copy('node_modules/angular-xeditable/dist/js/xeditable.min.js', 'public/js/lib/xeditable.js');


mix.js('resources/assets/js/front-end/app.js', 'public/js/app.js').version();

/* SCSS to CSS */
mix.sass('resources/assets/sass/front-end.scss', 'public/css/app.css')
    .options({
        processCssUrls: false
    })
    .version();
mix.sass('resources/assets/sass/back-end.scss', 'public/css/ng.css')
    .options({
        processCssUrls: false
    })
    .version();


mix.copyDirectory('resources/assets/sass/back-end/font-awesome/fonts', 'public/fonts/back-end');
mix.copyDirectory('resources/assets/img', 'public/img');
mix.copyDirectory('resources/assets/views', 'public/views');