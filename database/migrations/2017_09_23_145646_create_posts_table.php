<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->default(0)->comment('ID категории');
            $table->integer('author_id')->default(0)->comment('ID автора');
            $table->string('image')->nullable();
            $table->string('slug')->unique();
            $table->string('name');
            $table->string('short_desc')->nullable();
            $table->text('content');
            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->boolean('status')->default(0);
            $table->tinyInteger('meta_robots')->default(0)->comment('Мета-тег Robots');
            $table->timestamp('publish_up')->nullable()->comment('Начало публикации');
            $table->timestamp('publish_down')->nullable()->comment('Завершение публикации');
            $table->integer('hits')->default(0)->comment('Кол-во просмотров');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
