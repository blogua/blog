<?php

use Faker\Generator as Faker;

$factory->define(\App\Model\MapTag::class, function (Faker $faker, $attribute) {

    $attribute['tag_id'] = random_int(1, 20);

    return $attribute;
});
