<?php

use Faker\Generator as Faker;

$factory->define(\App\Model\Category::class, function (Faker $faker) {
    return [
        'author_id'     => random_int(1, 200),
        'image'         => $faker->imageUrl(300, 300, 'people'),
        'slug'          => $faker->slug,
        'name'          => $faker->text(random_int(20, 50)),
        'short_desc'    => $faker->realText(random_int(30, 50)),
        'content'       => $faker->realText(random_int(100, 500)),
        'meta_title'    => $faker->text(random_int(10, 255)),
        'meta_keyword'  => $faker->text(random_int(10, 255)),
        'meta_description' => $faker->text(random_int(10, 255)),
        'status'        => 1,
        'created_at'    => $faker->date(),
    ];
});
