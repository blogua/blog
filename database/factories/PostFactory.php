<?php

use Faker\Generator as Faker;

/**
 * $faker->imageUrl
 * https://lorempixel.com/
 */
$factory->define(\App\Model\Post::class, function (Faker $faker) {

    return [
        'category_id'   => $faker->randomDigitNotNull,
        'author_id'     => random_int(1, 200),
        'image'         => $faker->imageUrl(640, 480, 'people'),
        'slug'          => $faker->slug,
        'name'          => $faker->text(random_int(20, 50)),
        'short_desc'    => $faker->realText(random_int(10, 255)),
        'content'       => '<p>'.$faker->realText(random_int(1000, 10000), 3).'</p>',
        'meta_title'    => $faker->text(random_int(10, 255)),
        'meta_keyword'  => $faker->text(random_int(10, 255)),
        'meta_description' => $faker->text(random_int(10, 255)),
        'status'        => 1,
        'created_at'    => $faker->dateTimeBetween('-1 years')->format('Y-m-d H:i:s'),
        //'created_at'    => $faker->date(),
    ];
});
