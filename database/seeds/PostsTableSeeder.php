<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #$posts = factory(\App\Model\Post::class, 3)->make();

        factory(\App\Model\Post::class, 300)->create()->each(function ($u) {
            #$u->posts()->save(factory(App\Post::class)->make());
            factory(\App\Model\MapTag::class, 1)->create([
                'post_id'   => $u->id,
            ]);
        });
    }
}
