<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('roles')->insert([
            ['id'=>1,'name'=>'Администратор'],
            ['id'=>2,'name'=>'Редактор'],
            ['id'=>3,'name'=>'Автор'],
        ]);
    }
}
