<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name'    => 'Вадим',
            'last_name'     => 'Швець',
            'role_id'       => 1,
            'email'         => '1074746@gmail.com',
            'password'      => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'first_name'    => 'Oleg',
            'last_name'     => 'Tsegelnyk',
            'role_id'       => 1,
            'email'         => 'tsegelnyk@gmail.com',
            'password'      => bcrypt('123456'),
        ]);

        factory(\App\User::class, 200)->create();
    }
}
