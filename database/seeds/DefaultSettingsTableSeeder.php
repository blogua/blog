<?php

use Illuminate\Database\Seeder;

class DefaultSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
           [
               'name'   => 'name',
               'value'  => 'Etna'
           ], [
               'name'   => 'description',
               'value'  => 'Etna Description'
           ]
        ]);
    }
}
